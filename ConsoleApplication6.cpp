

#include <iostream>
#include <string>

using namespace std;

class Player 
{
private:
    string name;
    int score;

public:
    void SetName(string _name)
    {
        name = _name;
    }
    
    string GetName()
    {
        return name;
    }

    void SetScore(int _score)
    {
        score = _score;
    }

    int GetScore()
    {
        return score;
    }
};

void quicksort(Player* l, Player* r) {
  
    int z = (l + (r - l) / 2)->GetScore();
    Player* ll = l, * rr = r;
    while (ll <= rr) {
        while (rr->GetScore() < z) rr--;
        while (ll->GetScore() > z) ll++;
        if (ll <= rr) {
            swap(*ll, *rr);
            ll++;
            rr--;
        }
    }
    if (l < rr) quicksort(l, rr);
    if (ll < r) quicksort(ll, r);
}

int main()
{
    int n;
    cout << "Enter number of players: ";
    cin >> n;
    Player* players = new Player[n];
    
    for (int i = 0; i < n; i++)
    {
        cout << "Enter name and score for player " << i + 1 << ": ";
        string name;
        int score;
        cin >> name;
        cin >> score;
        players[i].SetName(name);
        players[i].SetScore(score);
    }

    cout << "\n";

    quicksort(players, &players[n - 1]);

    for (int i = 0; i < n; i++)
    {
        cout << players[i].GetName() << " " << players[i].GetScore() << "\n";
    }

    delete[] players;
    players = nullptr;
   
}


